import React from 'react';

class DisplayNameComponent extends React.Component{
    render(){
        return (
            <div className='App'>
                <p>First name is: {this.props.firstName}</p>
            </div>
        )
    }
}


export default DisplayNameComponent;