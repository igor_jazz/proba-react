import React from 'react';

const MyFirstReactComponent = (props) => {
    return(
        <div>
            This is my first React try; {props.myprop}
        </div>
    )
}
export default MyFirstReactComponent;