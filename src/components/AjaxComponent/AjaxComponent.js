import React from 'react';

class AjaxComponent extends React.Component {
    constructor(){
        super();
        this.state={
            post:{}
        
        }
           
    }
    componentDidMount() {
        fetch('http://jsonplaceholder.typicode.com/posts/1').then(
            result => result.json()
        ).then((data => {

            this.setState({
                post:data
            })
        }))
    }
        render(){
            return(
                <div>
                    <hr/>
                    <h1>{this.state.post.title}</h1>
                    <p>{this.state.post.body}</p>
                </div>
            )
        }
}

export default AjaxComponent;