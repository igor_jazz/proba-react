import React from 'react';

class Clock extends React.Component{
    constructor(){
        super();

        this.state = {
            time: new Date()
        }
    }
    componentDidMount(){
        setInterval(() =>{
            this.setState({
                time: new Date()
            })
        })
    }
    render(){
        
        
        return(
            <div>
                <div>
                    {this.state.time.toLocaleTimeString()}
                </div>
            </div>
        )
    }
}

export default Clock;