import React from 'react';

import './App.css';
import MyFirstComponent from './components/MyFirstComponent/myMyFirstComponent';
import MySecondComponent from './components/MySecondComponent/MySecondComponent';
import MyFirstReactComponent from './components/MyFirstReactComponent/MyFirstReactComponent';
import MySecondReactComponent from './components/MysecondReactComponent/MySecondReactComponent';
import DisplayNameComponent from './components/DisplayNameComponent/DisplayNameComponent';
import AjaxComponent from './components/AjaxComponent/AjaxComponent';

function App() {
  return (
   <div className='App'>
    <AjaxComponent></AjaxComponent>
     {/* <MyFirstComponent myporp='my first prpo'></MyFirstComponent>
     <MySecondComponent></MySecondComponent>
     <MySecondComponent></MySecondComponent>
     <MySecondComponent></MySecondComponent>

     <MyFirstReactComponent></MyFirstReactComponent>
     <MySecondReactComponent></MySecondReactComponent>
     <DisplayNameComponent firstName='riste' lastName='samardjiev'></DisplayNameComponent> */}
   </div>
  );
}

export default App;
